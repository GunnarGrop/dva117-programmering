#include "filemanagement.h"
#include "memorymanagement.h"
#include "tryInput.h"

void saveListToFile(shoppingList *pGroceries){
    if(pGroceries->itemsInList == 0){
        printf("\nYou can't save an empty list.\n");
    }else{
        printf("\nNew file name: ");
        char fileName[FILENAME_SIZE];
        inputFileName(fileName);

        FILE *fp = fopen(fileName, "w");
        if(fp == NULL){
            printf("\nERROR: Failed to open file.\n");
        }else{
            fprintf(fp, "%d\n", pGroceries->itemsInList);
            for(int i = 0; i < pGroceries->itemsInList; i++){
                fprintf(fp, "%s\n", pGroceries->ShoppingListItem[i].item);
                fprintf(fp, "%f\n", pGroceries->ShoppingListItem[i].amount);
                fprintf(fp, "%s\n", pGroceries->ShoppingListItem[i].unit);
            }
            fclose(fp);
            printf("\nList saved as: \"%s\"\n", fileName);
        }
    }
    printf("\n");
}

void loadListFromFile(shoppingList *pGroceries){
    printf("\nFile to load: ");
    char fileName[FILENAME_SIZE];
    inputFileName(fileName);

    FILE *fp = fopen(fileName, "r");

    if(fp == NULL){
        printf("\nERROR: Failed to open file.\n");
    }else{
        free(pGroceries->ShoppingListItem);
        fscanf(fp, "%d\n", &pGroceries->itemsInList);
        pGroceries->ShoppingListItem = tryToAllocatieMemory(pGroceries->itemsInList);

        for(int i = 0; i < pGroceries->itemsInList; i++){
            fscanf(fp, "%s\n", pGroceries->ShoppingListItem[i].item);
            fscanf(fp, "%f\n", &pGroceries->ShoppingListItem[i].amount);
            fscanf(fp, "%s\n", pGroceries->ShoppingListItem[i].unit);
        }
        printf("\nLoaded list \"%s\"\n", fileName);
        fclose(fp);
    }
    printf("\n");
}
