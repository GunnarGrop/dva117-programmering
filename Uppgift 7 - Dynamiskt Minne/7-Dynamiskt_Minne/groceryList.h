#ifndef _GROCERYLIST_H_
#define _GROCERYLIST_H_

#include <stdio.h>
#define ITEMSIZE 20
#define UNITSIZE 10

typedef struct{
    char item[ITEMSIZE];
    float amount;
    char unit[UNITSIZE];
}groceryItem;

typedef struct{
    groceryItem *ShoppingListItem;
    int itemsInList;
}shoppingList;

//Adds item name, amount, and unit to list, per user input
void AddItemToList(shoppingList *pGroceries);
//Prints the whole list, in numeric order
void printList(shoppingList *pGroceries);
//Overwrites the removed position with item on the next index
void removeItemFromList(shoppingList *pGroceries);
//Askes uesr for new amount for item on chosen index
void editItemInList(shoppingList *pGroceries);

#endif
