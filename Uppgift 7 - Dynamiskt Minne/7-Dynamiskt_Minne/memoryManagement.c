#include "memorymanagement.h"

groceryItem* tryToAllocatieMemory(unsigned long int sizeOfNewMemBlock){
    groceryItem *tmp;
    tmp = (groceryItem*) calloc(sizeof (groceryItem), sizeOfNewMemBlock);
    if(tmp != NULL){
        return tmp;
    }else{
        printf("ERROR: allocation of memory failed.\n");
        return NULL;
    }
}

void tryToReallocateMemory(groceryItem **oldMemBlock, unsigned long int sizeOfNewMemBlock){
    groceryItem *tmp = (groceryItem*) realloc(*oldMemBlock, sizeof(groceryItem) * sizeOfNewMemBlock);
    if(tmp != NULL){
        *oldMemBlock = tmp;
    }else{
        printf("ERROR: reallocation of memory failed.\n");
    }
}
