#ifndef _FILEMANAGEMENT_H_
#define _FILEMANAGEMENT_H_

#include <stdio.h>
#include "groceryList.h"
#define FILENAME_SIZE 23

//Saves the number of items, followed by all items, to a file
void saveListToFile(shoppingList *pGroceries);
//Loads all informartion from a saved file to the current state
void loadListFromFile(shoppingList *pGroceries);

#endif
